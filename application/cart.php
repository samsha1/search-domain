<!DOCTYPE html>
<html>
<head>
	<title>Your Cart</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">

	<!-- jquery must be above javascript -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/jquery/jquery-3.1.1.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
</head>
<body>
		<div class="container">
			<div class="row">
				<div class="col-lg-12"><center><h1>You cart list</h1></center></div>
				<div col-lg-12>
					<table class="table">
						<thead>
							<tr>
								<th>Serial no.</th>
								<th>Domain name</th>
								<th>remove</th>
							</tr>
						</thead>
					
					<tbody>
					<?php
						if(!empty($_SESSION["shopping_cart"])){
							$count = 1;
							foreach ($_SESSION["shopping_cart"] as $key => $value) {
					?>
								<tr>
									<td><?=$count;?></td>
									<td><?=$value['domain_name'];?></td>
									<td value="<?=$value['index'];?>"><button class="btn btn-sm btn-danger remove">remove</button></td>
								</tr>
					<?php
							$count++;
							}
					
						}else{
							echo "<tr><td colspan='3'>no cart</td><tr>";
						}
					?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
</body>
</html>

<script type="text/javascript">
	$(document).ready(function(){
		$('.remove').on('click',function(){
			var index = $(this).parent().attr('value');
			$.ajax({
	          type:'POST',
	          url:'<?=base_url('sdomain/remove_list');?>',
	          data:{'index':index},
	          success:function(data){
	           //alert("success");
	           location.reload(0);
	          },
	          error:function(){
	            alert("unsuccess");
	          }
	        });
		});
	});
</script>