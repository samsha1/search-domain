<!DOCTYPE html>
<html>
<head>
	<title>Your Cart</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">

	<!-- jquery must be above javascript -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/jquery/jquery-3.1.1.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
	<style>
	.pbtn,.pbtn:hover,.pbtn:active,.pbtn:focus,.pbtn:active:focus{
		background-color:#2FB682;
		border-color: none;
		color:white;
		border-radius: 0px;
	}
</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12"><center><h1>Your cart list</h1></center></div>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-9">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="row">
									<div class="col-md-3">Product</div>
									<div class="col-md-3">Term</div>
									<div class="col-md-3">Unit Price</div>
									<div class="col-md-3">Subtotal</div>
								</div>
							</div>
							<?php
							if(!empty($_SESSION["shopping_cart"])){
								$count=0;
								foreach ($_SESSION["shopping_cart"] as $key => $value) {
									?>
									<div class="panel-body">


										<div class="col-md-3"><?=$value['domain_name'];?></div>
										<div class="col-md-3">
											<select class="form-control" style="width:60%;" onchange="selectterm(this.id,this.options[this.selectedIndex].value)" id="<?= $count;?>">
												<option value="1">1 Year</option>
												<option value="2">2 Years</option>
												<option value="3">3 Years</option>
												<option value="4">4 Years</option>
												<option value="5">5 Years</option>
											</select>
										</div>
										<div class="col-md-3">$<div id="billamt<?= $count;?>" style="display: inline-block;" class="a"><?=$value['billprice'];?></div>/yr 1st year</div>
									<div class="col-md-3" value="<?=$value['domain_name'];?>" >$<div class="addamt" style="display: inline-block;"><?=$value['billprice'];?></div><a class="remove" style="text-decoration: none; cursor: pointer;"> [remove]</a></div>
									</div>
									<?php
									$count++;
								}

							}else{
								echo "<td colspan='3'><center><h4>No Items Available.</h4></center></td>";
							}
							?>
							
						</div>
					</div>
					<div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-body">
								<div><h4>Total Amount:$<span id="totalamt"></span></h4></div>
								<div><hr></div>
								<div><button class="btn btn-primary btn-lg pbtn radoff-btn">Proceed To Checkout ></button>	</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</body>
</html>

<script type="text/javascript">
	var sum = 0.0;
	$(document).ready(function(){
		$('.remove').on('click',function(){
			var index = $(this).parent().attr('value');
			var $ele = $(this).parent().parent();
			$.ajax({
				type:'POST',
				url:'<?=base_url('home/remove_list');?>',
				data:{'domain':index},
				success:function(data){
	           //alert("success");
	           $ele.fadeOut().remove();
	           //location.reload(0);
	       },
	       error:function(){
	       	alert("unsuccess");
	       }
	   });
		});
	});

	function selectterm(classnam,year){
		var cd=document.getElementById('billamt'+classnam).innerHTML;
		document.getElementsByClassName("addamt")[classnam].innerHTML=parseInt(year)*parseFloat(cd);
		var lo=0;
		var pm=0.0;
		 $('.addamt').each(function()
		{
			var am=parseFloat(document.getElementsByClassName("addamt")[lo].innerHTML);
			pm += parseFloat(am);
			lo++;
			//alert(pm);

		});
		 document.getElementById('totalamt').innerHTML=pm.toString().substr(0,5);
		
		 //$('#addamt').html(parseInt(year)*parseInt(cd));
		}

		
		$('.a').each(function()
		{
			sum += parseFloat($(this).html());
		});
		document.getElementById('totalamt').innerHTML=sum;
	</script>