<html>
<head>
	<title>Check Domain Avaibality</title>
	  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/bootstrap/css/bootswatch.css">

	<!-- jquery must be above javascript -->
	<script type="text/javascript" src="<?php echo base_url();?>assets/jquery/jquery-3.1.1.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<br>
			<form class="form-horizontal" action="<?php echo base_url();?>home/checkdomain" method="GET" >

		<div class="form-group">
			<label for="dname" class="col-sm-3 control-label">Domain Name:</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="domainname" required>
			</div>
			
		</div>
		<div class="form-group">
			<div class="col-sm-3"></div> 
			<div class=" col-md-9">
				<button type="submit" class="btn btn-primary busSearch radoff-btn btn-md ogin">
				Check Domain</button>
			</div>
		</div>
	</form>
		</div>
	</div>
</body>
</html>

