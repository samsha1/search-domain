<?php 
Class Sdomain extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->view('fdomain');
		
	}
	public function destroysess(){
		$domainname=$this->input->get('domainname');
		$this->load->library('session');
		$this->session->sess_destroy();
		redirect(base_url().'sdomain/checkdomain?domainname='.$domainname,'refresh');
	}

	public function checkdomain(){
		$this->session->sess_destroy();
		$domainname=$this->input->get('domainname');

		$token = $this->login();

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://dev.api.nepallink.info/v1/domain/api/client/domain/check?domainname=".$domainname,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"authorization: Bearer ".$token,
				"cache-control: no-cache",
				"postman-token: d5395408-f90d-8acb-182a-0de719a477a2"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {

			//echo $response;
			$jsonde=json_decode($response,true);	 
			 $this->load->view('domainlist',array('yourdetail'=>$jsonde));
			 // foreach ($jsonde as $key) {
			 //  echo $key['domain'].'&nbsp; &nbsp; &nbsp;';
	  	// 	  echo $key['status'].'&nbsp; &nbsp; &nbsp;';
			 //  }

			
			
		}

	}

	function login(){

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://dev.api.nepallink.info/v1/login",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "{\n\t\"email\":\"samrat.shakya@nepallink.net\",\n\t\"password2\":\"samrat12345\"\n}",
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json",
				"postman-token: d0f505fd-7968-b9c4-2a73-e8cb3a4a14c8"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
	  // echo $response;
			$jsonde=json_decode($response,true);
			return $jsonde['token'];


		// $
	 //  $c= $.parseJSON($response);
	 //  echo $c['token'];
		}
	}

	public function cart(){
		$this->load->view('cart');
	}


	public function add_cart(){
		$domain = $this->input->post('domain');
		if(isset($_SESSION['shopping_cart'])){
			$item_domain_name = array_column($_SESSION["shopping_cart"], "domain_name");
			if(!in_array($domain,$item_domain_name)){
				$count = count($_SESSION["shopping_cart"]);
				$item_array = array(
					'index' => $count,
					'domain_name' => $domain
				);
				$_SESSION["shopping_cart"][$count]= $item_array;
				echo "added";
			}else{
				echo "already addded to cart"; 
			}
		}else{
			$item_array = array(
				'index' => '0',
				'domain_name' => $domain
			);
			$_SESSION["shopping_cart"][0] = $item_array;
			echo "added";
		}
	}

	public function remove_list(){
		$domain = $this->input->post('domain');
		foreach ($_SESSION["shopping_cart"] as $key => $value) {
			if($value['domain_name']==$domain){
				unset($_SESSION['shopping_cart'][$key]);
				echo "sremove";
			}
		}
	}

}
?>