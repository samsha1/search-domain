<html>
<head>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/backend/bootstrap/css/bootswatch.css">

  <!-- jquery must be above javascript -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/jquery/jquery-3.1.1.min.js"></script>

  <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
  <style type="text/css">
  .added{
    font-size:15px;color:green;
  }
  .remove{
    cursor: pointer;
    text-decoration: none;
    margin-left: 10px;
  }
</style>
</head>
<body style="background-color:#F6F6F6;">

  <div class="container" >
    <div class="row">
      <br>
      <div class="col-md-9">
        <form class="form-horizontal" action="<?php echo base_url();?>sdomain/checkdomain" method="GET" >

          <div class="form-group">
            <label for="dname" class="col-sm-3 control-label">Domain Name:</label>
            <div class="col-sm-6">
              <input type="text" class="form-control" name="domainname" required>
            </div>
            <div class="col-sm-3">
             <button type="submit" class="btn btn-primary busSearch radoff-btn btn-md ogin">
             Search Again</button>

           </div>
         </div>
       </form>
     </div>
     <div class="col-md-3"> <a target="_blank" href="<?=base_url('sdomain/cart');?>" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-shopping-cart" style="font-size: 18px;"></span> Continue to Cart</a><br>
      <div id="messi"><span id="num">0</span> domain selected</div>
      <a  href="<?php echo base_url();?>sdomain/destroysess?domainname=<?php echo $_GET['domainname'];?>"> Destroy Session(only for test)</a>
    </div>
  </div>
  <div class="row">


    <div class="col-md-12">
      <div class="row">
        <?php 
        $i=0;
        foreach ($yourdetail as $val)
          if($i==0){
           if($val['status']=='available'){
            ?>
            <h4>Congratulation Your domain is available</h4>
            <?php
          }else{ ?>
          <h4>Sorry,<b><?php echo $_GET['domainname'];?></b> is already taken.</h4>
          <?php }
          $i++;
        } 
        ?>
        <div id="messi"></div>
        <div class="panel panel-default">
          <div class="panel-heading">
           <div class="row">
            <div class="col-md-4">Domain Name</div>
            <div class="col-md-4">Status</div>
            <div class="col-md-4">Remarks</div>
          </div>
        </div>
        <?php
        foreach ($yourdetail as $key) {
          ?>

          <div class="panel-body">

           <div class="col-md-4">
             <p><b><?php echo @$key['domain'];?></b></p>
           </div>
           <div class="col-md-4">

             <?php if(@$key['status']!='available'){?>
             <p><b>Unavailable</b></p>
             <?php }else{
              echo '<b>'.'Available'.'</b>';
            }?>
          </div>
          <?php if(@$key['status']=='available'){?>
          <div class="col-md-4">
           <!-- <a href="#" id="" class="btn btn-sm btn-primary tic_cancel">Add To Cart</a> -->
           <button class="btn btn-sm btn-primary tic_cancel">Add To Cart</button>
         </div>
         <?php }?>

       </div>
       <hr>
       <?php
     }
     ?>
   </div>
 </div>  
</div>

<div class="col-md-1"></div>
</div>
</div>

</body>
</html>
<script>
  
  $(function(){
    $('#messi').hide();
    jQuery('body').on('click', '.tic_cancel', function () {
      var domain = $(this).parent().parent().children("div:first").children().children().html();
      var ele = $(this).parent();
      $(this).replaceWith('<b>'+'Verifying Avaibility...'+'</b>');
      $.ajax({
        type:'POST',
        url:'<?=base_url('sdomain/add_cart');?>',
        data:{'domain':domain},
        success:function(data){
          if(data=="added"){
            ele.html('<div class="added"><span class="glyphicon glyphicon-ok"></span> ADDED <a class="remove"><span class="glyphicon glyphicon-remove"></span> Remove</a></div>');
                      // $(this).parent().html('<p>'+'Added'+'</p>');
                      $('#messi').show();
                      var cd=document.getElementById('num').innerHTML;
                      $('#num').html(parseInt(cd)+1);
                      //$('#num').html(successCount++);
                      //$('#messi').html('1 domain selected');
                    }else{
                      alert("Domain is not available");
                    }
                  }
                });
    });
    jQuery('body').on('click', '.remove', function () {
      var domain = $(this).parent().parent().parent().children("div:first").children().children().html();
      var ele = $(this).parent().parent();
      $.ajax({
        type:'POST',
        url:"<?=base_url('sdomain/remove_list');?>",
        data:{'domain':domain},
        success:function(data){ 
         if(data=="sremove"){
          var cd=document.getElementById('num').innerHTML;
           $('#num').html(parseInt(cd)-1);
           ele.html('<button class="btn btn-sm btn-primary tic_cancel">Add To Cart</button>');
         }else{
          alert("can't delete the row");
        }
      }
    });
    });

  });
</script>



