<?php 
Class Home extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->view('fdomain');
		
	}
	
	public function destroysess(){
		$domainname=$this->input->get('domainname');
		@session_start();
		session_unset('shopping_cart');
		redirect(base_url().'home/checkdomain?domainname='.$domainname,'refresh');
	}

	public function checkdomain(){
		@session_start();
		session_unset('shopping_cart');
		$domainname=$this->input->get('domainname');

		$token = $this->login();
		$billamt=$this->billing($domainname);

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://dev.api.nepallink.info/v1/domain/api/client/domain/check?domainname=".$domainname,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"authorization: Bearer ".$token,
				"cache-control: no-cache",
				"postman-token: d5395408-f90d-8acb-182a-0de719a477a2"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {

			
			$jsonde=json_decode($response,true); 
			 // $mydata=array($do,$stat);
			$this->load->view('domainlist',array('yourdetail'=>$jsonde,'bill'=>$billamt));
			
		}

	}

	private function billing($domainname){
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://dev.api.nepallink.info/v1/billing/api/client/addOrder",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "{\"currency\":\"USD\",\"domain\":[\"$domainname\"],\"regperiod\":[\"1\"],\"domaintype\":[\"register\"]}",
			CURLOPT_HTTPHEADER => array(
				"authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJmaXJzdG5hbWUiOiJEaW5lc2giLCJncm91cGlkIjoiMCIsImlzcyI6IjU0YzU1YTk4YTU2NzRhNDJiNDMxMjAzMzAxMTVlYjY2IiwiZXhwIjoxNTA2Mjc2MTY4LCJ1c2VyaWQiOiIyOTMiLCJjdXJyZW5jeV9jb2RlIjoiVVNEIiwiZW1haWwiOiJtZWRpbmVzaGthdHdhbEBnbWFpbC5jb20iLCJsYXN0bmFtZSI6IkthdHdhbCJ9.3TpqAfjGJGSsZ7Fve8Pc7tkLxsLDKisOhxomjQQTKww",
				"cache-control: no-cache",
				"content-type: application/json",
				"postman-token: efb85a55-83b9-a77e-bb7b-d5ef55a96604"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			//echo $response;
			$jsonde=json_decode($response,true);

			return $jsonde['total'];
		}
	}

	private function login(){

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://dev.api.nepallink.info/v1/login",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "{\n\t\"email\":\"samrat.shakya@nepallink.net\",\n\t\"password2\":\"samrat12345\"\n}",
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json",
				"postman-token: d0f505fd-7968-b9c4-2a73-e8cb3a4a14c8"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
	  // echo $response;
			$jsonde=json_decode($response,true);
			return $jsonde['token'];


		// $
	 //  $c= $.parseJSON($response);
	 //  echo $c['token'];
		}
	}

	public function cart(){
		@session_start();							
		$this->load->view('cart');
	}


	public function add_cart(){
		@session_start();
		$domain = $this->input->post('domain');
		$bill=$this->billing($domain);
		if(isset($_SESSION['shopping_cart'])){
			
			$item_domain_name = array_column($_SESSION["shopping_cart"], "domain_name");
			if(!in_array($domain,$item_domain_name)){
				
				$count = count($_SESSION["shopping_cart"]);
				$item_array = array(
					'index' => $count,
					'domain_name' => $domain,
					'billprice'=>$bill
				);
				//$_SESSION["shopping_cart"][$count]=$item_array;
				array_push($_SESSION["shopping_cart"],$item_array);
				echo "added";
			}else{
				echo "already addded to cart"; 
			}
		}else{
			$item_array = array(
				'index' => '0',
				'domain_name' => $domain,
				'billprice'=>$bill
			);
			$_SESSION["shopping_cart"][0] = $item_array;
			echo "added";
		}
	}

	public function remove_list(){
		@session_start();
		$domain = $this->input->post('domain');
		foreach ($_SESSION["shopping_cart"] as $key => $value) {
			if($value['domain_name']==$domain){
				unset($_SESSION['shopping_cart'][$key]);
				echo "sremove";
			}
		}
	}

}
?>